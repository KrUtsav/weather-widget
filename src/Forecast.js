import React, { Component } from 'react';
import './application.css';

class Forecast extends Component {
    state = { 
        data: "null"
     }

     static getDerivedStateFromProps(props, state) {
        if(state.data !== props.data)
            return (state.data = props.data);
        
        else
            return null;
     }

    render() { 
        let item = 0, dayCount = 0;
        const template = [];

        let notation = "";

        if(this.props.notation == 'I')
            notation = " °F ";

        else
            notation = " °C "

        const date = new Date();
        const dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

        dayCount = date.getDay();

        for (item = 1; item <= 7; item = item + 1)
        {
            dayCount = (dayCount + 1) % 7;
            template.push(
            <div key = {item} className = 'Forecast'>
                    <p className = "day">{dayNames[dayCount]}</p>
                    <img className = "icon" src = {`./icons/${this.state.data[item].weather.icon}.png`} />
                    <div className = "MinMaxTemp">
                            {this.state.data[item].max_temp + notation + this.state.data[item].min_temp + notation}
                    </div>
                </div>
            )

        }

        return ( 
            <div className = "Box">
                {template}
                <div className = "Clearing"></div>
            </div>
         );
    }
}
 
export default Forecast;