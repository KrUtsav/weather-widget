import React, { Component } from 'react';
import Header from './Header';
import './application.css';

class App extends Component {

    render() { 
    
      return ( 
        <div className = "Content">
          <Header />
        </div>
      );
  
    }
  }
 
export default App;