import React, { Component } from 'react';
import './application.css';

class ToedayWeather extends Component {
    state = { 
        data: "null"
     }

     static getDerivedStateFromProps(props, state) {
        if(state.data !== props.data)
            return (state.data = props.data);
        
        else
            return null;
     }
    
    render() { 

        let notation = "";

        if(this.props.notation == 'I')
            notation = " °F ";

        else
            notation = " °C "

        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];

        const dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Friday", "Saturday"]

        const date = new Date();

        return ( 
            <div className = "Box">
                <div className = "left-side">
                    <div className = "CityName">{this.state.data[0].city_name}</div>
                    <div>{ dayNames[date.getDay()] + ", " + date.getDate() + " " + monthNames[date.getMonth()] }</div>

                    <div>
                        <label name = 'Present Weather'>{this.state.data[0].weather.description}</label>
                    </div>
                    <div className = "Inline">
                        <img className = "image" src = {`./icons/${this.state.data[0].weather.icon}.png`} />
                        <label className = "temperature" name = 'Present Weather'>{this.state.data[0].temp + notation}</label>
                        <div className = "Clearing"></div>
                    </div>
                    
                </div>

                <div className = "right-side">

                    <div>
                        Wind:
                        <label name = 'Present Weather'>{  this.state.data[0].wind_spd.toFixed(2) + " kph " + this.state.data[0].wind_cdir}</label>
                    </div>
                    <div>
                        Precipitation:
                        <label name = 'Present Weather'>{  this.state.data[0].precip + " %"}</label>
                    </div>
                    <div>
                        Humidity:
                        <label name = 'Present Weather'>{  this.state.data[0].rh + " %"}</label>
                    </div>
                </div>

                <div className = "Clearing"></div>
            </div>
         );
    }
}
 
export default ToedayWeather;