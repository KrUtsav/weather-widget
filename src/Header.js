import React, { Component } from 'react';
import ToedayWeather from './TodayWeather';
import axios from 'axios';
import Forecast from './Forecast';
import './application.css';

class Header extends Component {
  state = { 
    City: '',
    data: {},
    forecastData: {},
    forecastOpen: false,
    open: false,
    currentData: {},
    forecastCurrentData: {},
    longitude: '',
    latitude: '',
    openFirst: false,
    openForecastFirst: false, 
    prevMetric: '',
    metric: 'M'
   }

   componentDidUpdate = (prevState, prevProps) => {
    
      if(this.state.metric !== this.state.prevMetric)
      {
    
        if(this.state.openForecastFirst && this.state.openFirst)
        {
          this.getCurrentData();
          this.getCurrentForecastData();
        }

        else
        {
          this.getSearchedData();
          this.getForecastData();
        }

        this.setState({
          prevMetric: this.state.metric
        })
      }

   }

   componentDidMount () {
     this.getlocation();
   }

   getlocation = () => {
     if(navigator.geolocation) {
       navigator.geolocation.getCurrentPosition(this.getCoordinates);
     }
   }

   getCoordinates = (position) => {
    this.setState({
      latitude: position.coords.latitude,
      longitude: position.coords.longitude
    })

    this.getCurrentData();
    this.getCurrentForecastData();
   }

   getCurrentData = () => {
    axios.get(`https://api.weatherbit.io/v2.0/current?lat=${this.state.longitude}&lon=${this.state.latitude}&units=${this.state.metric}&key=a51a0ef33e404743b690a1198fc0cf98`)
    .then(res => {
        this.setState({
          currentData: res.data,
          openFirst: true
        });
      })
    .catch(error => console.log(error))
   }

   getCurrentForecastData = () => {
    axios.get(`https://api.weatherbit.io/v2.0/forecast/daily?lat=${this.state.longitude}&lon=${this.state.latitude}&units=${this.state.metric}&key=a51a0ef33e404743b690a1198fc0cf98`)
    .then(res => {
      this.setState({
        forecastCurrentData: res.data,
        openForecastFirst: true
        })
    })
    .catch(error => console.log(error)) 
   }

   getSearchedData = () => {
    axios.get(`https://api.weatherbit.io/v2.0/current?city=${this.state.City}&units=${this.state.metric}&key=a51a0ef33e404743b690a1198fc0cf98`)
    .then(res => {
      this.setState({
        data: res.data,
        open: true
      })
      console.log(this.state.data)
    })
    .catch(error => console.log(error))

   }

   getForecastData = () => {
    axios.get(`https://api.weatherbit.io/v2.0/forecast/daily?city=${this.state.City}&units=${this.state.metric}&key=a51a0ef33e404743b690a1198fc0cf98`)
    .then(res => {
      console.log(res.data);
      this.setState({
        forecastData: res.data,
        forecastOpen: true
        })
    })
    .catch(error => console.log(error))
   }

   addCity = (e) => {
    this.setState({
      City: e.target.value
    })
   }

   submitData = () => {
      this.setState({
        openFirst: false,
        openForecastFirst: false
      }, () => {
        console.log(this.state.openFirst, " ", this.state.openForecastFirst);
      })

      this.getSearchedData();  
      this.getForecastData();
   }

   onSelect = (e) => {
     this.setState({
      prevMetric: this.state.metric,
       metric: e.target.value
     }, () => {
       console.log(this.state.metric, " ", this.state.prevMetric)
     })
   }

  render() {
    return ( 
       <div>
         <div> 
          <div className = "Search">
            <input type = "text" placeholder = "enter city name" value = {this.state.City} onChange = {(e) => this.addCity(e)} />
            <input type = "button" className = "button" onClick = {() => this.submitData()} value = "Submit"/>
                        
          </div>
          <div className = "Result">
            <div className = "SearchBox">
              {this.state.open && <ToedayWeather data = {this.state.data} notation = {this.state.metric} />}
              {this.state.openFirst && <ToedayWeather data = {this.state.currentData} notation = {this.state.metric} />}
            </div>
          </div>
        </div>  
        <div className = "Clearing"></div>
          
            {this.state.openForecastFirst && <Forecast data = {this.state.forecastCurrentData} notation = {this.state.metric} />}
            {this.state.forecastOpen && <Forecast data = {this.state.forecastData} notation = {this.state.metric} />}
          
          <div className = "metric">
            Choose another metric system
          <select className = "selection" value = {this.state.metric} onChange = {(e) => {this.onSelect(e)}}>
            <option value = 'M'>Metric</option>
            <option value = 'I'>Imperial</option>
          </select>
          </div>

       </div>        
     );
  }
}
 
export default Header;